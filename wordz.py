import os
import sys

def get_args(args):
    """ Given a list of arguments , returns the first element and raises error if list is more than size 1 """
    
    # Check only one arg file is supplied
    if len(args) < 1:
        raise ValueError('File Name not provided')
    elif len(args) > 1:
        raise ValueError('More than one argument provided')
    input_file = args[0]
    return input_file

def find_largest_word(input_file):
    """ Given a filename, path, returns ths largest word with in that file.
        Conditions:
            - File is text only (utf-8), else UnicodeError is raised
            - File should be readable, else PermissionError is raised

        Note:
            - 'asdff-_*1' string with special characters are also considered words.
            - Only condition that defines a word is strings seperated by space
     """
    largest_word_length = 0
    largest_word = None
    try:
        if os.stat(input_file).st_size == 0:
            return None

        with open(input_file, mode='r', encoding='utf-8') as f:
            for line in f:
                for word in line.split():
                    if (len(word) > largest_word_length):
                        largest_word_length = len(word)
                        largest_word = word
        return largest_word
    except IsADirectoryError:
        raise IsADirectoryError('Is a directory, not a file: {}'.format(input_file))
    except FileNotFoundError:
        raise FileNotFoundError('File Not Found, no such file: {}'.format(input_file))
    except PermissionError:
        raise PermissionError('Permission Denied, cannot read the file: {}'.format(input_file))
    except UnicodeError:
        raise UnicodeError('Wrong File Format, not a text file: {}'.format(input_file))
    
def transpose(word):
    """ Given a string, transpose of the input string is returned
        Conditions:
            - if None is provided, raises ValueError
            - if empty string is provided, raises ValueError
     """
    if not word:
        raise ValueError('Invalid word to Transpose')
    return word[::-1]
    
if __name__ == "__main__":
    """ # If this file is called."""
    args = (sys.argv[1:])
    try:
        file = get_args(args)
        largest_word = find_largest_word(file)
        if largest_word:
            transposed_largest_word = transpose(largest_word)
            print (largest_word)
            print (transposed_largest_word)
    except Exception as e:
        sys.exit(e)

