import os
import struct
import pytest
import wordz
import subprocess

def test_transpose_a_word():
    """ To test if provided word is transposed """
    t_word = wordz.transpose('abcde')
    assert t_word == 'edcba'
    
def test_transpose_a_letter():
    """ To test if provided word/letter is transposed """
    t_word = wordz.transpose('a')
    assert t_word == 'a'

def test_transpose_nothing():
    """ To test if nothing (empty string) is provided , raises ValueError """
    with pytest.raises(ValueError, match='Invalid word to Transpose'):
        wordz.transpose('')

def test_transpose_none():
    """ To test if None is provided , raises ValueError """
    with pytest.raises(ValueError, match='Invalid word to Transpos'):
        wordz.transpose(None)

def test_get_args():
    """ To test commadline args """
    input_file = wordz.get_args(['input_filename'])
    assert input_file == 'input_filename'

def test_get_args_relative_path():
    """ To test commadline args for relative paths """
    input_file = wordz.get_args(['./Downloads/untitled.svg'])
    assert input_file == './Downloads/untitled.svg'

def test_get_args_with_no_args():
    """ To test commadline args, if nothing is provided """
    with pytest.raises(ValueError, match='File Name not provided'):
        wordz.get_args([])
    
def test_get_args_with_more_then_2_files():
    """ To test commadline args, if more than one argument is provided """
    with pytest.raises(ValueError, match='More than one argument provided'):
        wordz.get_args(['input_filename', 'input_filename'])

def test_find_largest_word(temp_file_with_data):
    """ To test largest word function """
    largest = wordz.find_largest_word(temp_file_with_data)
    assert largest == "abcdefg"

def test_find_largest_word_in_file_with_multi_word_line_data(temp_file_with_multi_word_line_data):
    """ To test largest word function, with muliple words in same line """
    largest = wordz.find_largest_word(temp_file_with_multi_word_line_data)
    assert largest == "abcdefghi"

def test_find_largest_word_return_first_largest(temp_file_with_repeating_large_words):
    """ To test largest word function, with 2 largest words, should return the first found word """
    largest = wordz.find_largest_word(temp_file_with_repeating_large_words)
    assert largest == "abcdefghi"

def test_find_largest_word_in_empty_file(temp_file_empty):
    """To test largest word function, if empty file is passed """
    largest = wordz.find_largest_word(temp_file_empty)
    assert largest == None

def test_find_largest_word_is_a_directory():
    """ To test largest word function, if directory is passed """
    directory = os.sep
    with pytest.raises(IsADirectoryError, match='Is a directory, not a file: {}'.format(directory)):
        wordz.find_largest_word(directory)


def test_find_largest_word_file_not_found():
    """ To test largest word function, if file is not found """
    non_exsistent_file = 'non_exsistent_file'
    with pytest.raises(FileNotFoundError, match='File Not Found, no such file: {}'.format(non_exsistent_file)):
        wordz.find_largest_word(non_exsistent_file)

def test_find_largest_no_permission(temp_file_with_no_read_permissions):
    """ To test largest word function, permission denied on a file """
    no_permission_file = temp_file_with_no_read_permissions
    with pytest.raises(PermissionError, match='Permission Denied, cannot read the file: {}'.format(no_permission_file)):
        wordz.find_largest_word(no_permission_file)

def test_find_largest_non_text_file():
    """ To test largest word function, if the provided file is not text file and is a pdf """
    pdf_file = os.path.join('test', 'test_file.pdf')
    with pytest.raises(UnicodeError, match='Wrong File Format, not a text file: {}'.format(pdf_file)):
        wordz.find_largest_word(pdf_file)

def test_end_to_end(temp_file_with_data):
    """ Test End to End (stdout), file with some words """
    p = subprocess.run(["python", "wordz.py", temp_file_with_data], stdout=subprocess.PIPE)
    assert p.stdout == b'abcdefg\ngfedcba\n'

def test_end_to_end_file_not_found():
    """ Test End to End (stderr), with non exsistent file"""
    non_exsistent_file = 'non_exsistent_file'
    p = subprocess.run(["python", "wordz.py", non_exsistent_file], stderr=subprocess.PIPE)
    assert p.stderr == b'File Not Found, no such file: non_exsistent_file\n'
