import pytest
import os

@pytest.fixture()
def temp_file_empty():
    """ Test Fixture to create a empty file """
    file = "test/test.temp"
    with open(file, "w") as f:
        f.write('')
    yield file
    os.remove(file)

@pytest.fixture()
def temp_file_with_no_read_permissions():
    """ Test Fixture to create a file without Read Permissions """
    no_permission_file = "test/test.temp"
    with open(no_permission_file, "w") as f:
        f.write('Something')
    os.chmod(no_permission_file, 0o333)
    yield no_permission_file
    os.remove(no_permission_file)

@pytest.fixture()
def temp_file_with_data():
    """ Test Fixture to create a file with data """
    file = "test/test.temp"
    with open(file, "w") as f:
        f.write('a\n')
        f.write('ab\n')
        f.write('abc\n')
        f.write('abcd\n')
        f.write('abcde\n')
        f.write('abcdef\n')
        f.write('abcdefg\n')
    yield file
    os.remove(file)

@pytest.fixture()
def temp_file_with_multi_word_line_data():
    """ Test Fixture to create a file with single line having multiple words """
    file = "test/test.temp"
    with open(file, "w") as f:
        f.write('a abcdefghi dgkk\n')
        f.write('ab skjdbs\n')
        f.write('abs df c\n')
        f.write('abcd fff\n')
        f.write('abcded dg \n')
        f.write('abcdef\n')
        f.write('abcdefgh\n')
    yield file
    os.remove(file)

@pytest.fixture()
def temp_file_with_repeating_large_words():
    """ Test Fixture to create a file with 2 different largest words of same length
        This i to test if the First largest is returned """
    file = "test/test.temp"
    with open(file, "w") as f:
        f.write('a abcdefghi dgkk\n')
        f.write('ab skjdbs\n')
        f.write('abs df c\n')
        f.write('abcd fff\n')
        f.write('abcded dg \n')
        f.write('abcdef\n')
        f.write('123456789\n')
    yield file
    os.remove(file)