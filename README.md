# Wordz

[![pipeline status](https://gitlab.com/vividhsv/wordz/badges/master/pipeline.svg)](https://gitlab.com/vividhsv/wordz/commits/master)
[![coverage report](https://gitlab.com/vividhsv/wordz/badges/master/coverage.svg)](https://gitlab.com/vividhsv/wordz/commits/master)

## [Test Report] (https://vividhsv.gitlab.io/wordz/report.html)
## [Test Coverage Report] (https://vividhsv.gitlab.io/wordz/)
## [CI/CD Pipeline] (https://gitlab.com/vividhsv/wordz/pipelines)

Program that prints the largest word in the file and also prints tranpose of the largest word

1. Read input from a file of words;
2. Find the largest word in the file
3. Transpose the letters in the largest word
4. show the largest word and the largest word transposed 
5. demonstrate positive and negative test cases
6. ensure you document code and instructions for building and running

## Assumptions 

1. Input file can has multiple words in a signle line. In the below example `abc` and `defe` are two different words.

Example:

```
abc defe
foo
bar
```

2. Words can have special characters.In the below example `def:de` is one signle word.

Example:

```
abc def:de
foo
bar
```

3. Text file of UTF-8 format are provided
4. Will not be tested for multiple Python versions and OS Platforms

## Prerequisites
1. Python is installed and available `Python 3.6` and above. For details on installation, please follow http://howtopython.org/en/latest/getting-started/#getting-python-installed
2. Pip in installed and available.
3. Pipenv is installed (optional, required if planning to use pipenv)

## Installing Dependencies (Required to run tests)
### Using requirements.txt 

If using [virtualenv](https://virtualenv.pypa.io/en/stable/) setup or system python

requirements.txt file is made available, install the Requirements using the below command

```
pip install -t requirements.txt
```

### Using [Pipenv](https://docs.pipenv.org/) (recommended, as it is cleaner )

If pipenv is not setup on your system, get the installation instructions [here](https://docs.pipenv.org/#install-pipenv-today) 

Once you have pipenv, go to  `wordz` directory and run

```
pipenv install
```

Then Pipenv would automagically locate the Pipfiles, create a new virtual environment and install the necessary packages.

# Getting Started

1. Clone the repo
2. cd in to the repo
3. Install the [Dependencies] (https://gitlab.com/vividhsv/wordz/edit/master/README.md#installing-dependencies) .Required to run tests.
4. Run the code as below

```
python wordz.py <filename or filepath> 
```

if running throw `pipenv` make make sure the `pipenv` shell is actiavte before running

```
pipenv shell
```

## Running Tests

```
pytest test -v
```

Note:

`test_find_largest_no_permission` in `test_unit_app.py` is mostly for linux sub-sytems(mac,linux). If you are using Windows, skip the test using the below command

```
pytest test -v -k 'not find_largest_no_permission'
```

if running through `pipenv` make make sure the `pipenv` shell is actiavte before running

```
pipenv shell
```

## Structure

`test` directory contains all the test
 
 `wordz.py` contains the source code
